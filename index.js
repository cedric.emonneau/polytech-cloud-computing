const http = require('http');
const port = process.env.PORT || 80;
const answerDelay = process.env.DELAY || 10000;
const memoryPerRequest = process.env.MEMORY || 1024*1024*100;

function allocateMemory(size){
	const numbers = size / 8;
	const arr = [];
	arr.length = numbers;
	for (let i = 0; i < numbers; i++) {
		arr[i] = i;
	}
	return arr;
}

let requestCounter = 0;

const server = http.createServer(function(req, res){
	const clientIpAddress = req.headers['x-real-ip'] ?? req.socket.remoteAddress;
	const requestId = requestCounter++;
	console.log(clientIpAddress+' > '+req.method+' '+req.url+' > '+requestId);

	const memoryBefore = process.memoryUsage();
	const allocatedRam = allocateMemory(memoryPerRequest);

	setTimeout(function(){
		res.writeHead(200, { "Content-Type": 'application/json; charset=utf-8' });
		res.write(JSON.stringify({
			time: Math.floor(new Date().getTime()/1000),
			memoryUsageAfter: process.memoryUsage(),
			memoryUsageAtStart: memoryBefore,
			fakeMemory: allocatedRam.length,
			requestId: requestId,
		}));
		res.end();
		console.log(clientIpAddress+' < '+requestId+'');

		if(global.gc) {
			console.log('> force garbage collector')
			global.gc();
		}
	}, answerDelay);
});

server.listen(port, '0.0.0.0', () => {
	console.log(`App is running on port ${port}`);
});

const intervalInfo = setInterval(function(){
	console.log(JSON.stringify(process.memoryUsage()))
}, 60*1000);

process.on('SIGTERM', () => {
	console.info('SIGTERM signal received.');
	server.close();
	clearInterval(intervalInfo);
});
