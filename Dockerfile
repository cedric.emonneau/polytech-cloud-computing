FROM node:18-alpine
WORKDIR /usr/src/app
COPY index.js .

CMD ["node","--expose-gc", "index.js"]
